# ML Flow Field Predictions

This subgroup contains the projects `Example U-Net` and `nacaTransformer` which showcase the application of the `AirfoilMNIST` dataset

## Example U-Net

`Example U-Net` contains code for an architecture initially proposed for another dataset, which was adapted to show the usage of our dataset.

## nacaTransformer

`nacaTransformer` includes a novel architecture we are developing.